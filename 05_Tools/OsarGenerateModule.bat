
set "basePath=%~dp0.\..\02_Software\"
set "cfgFilePath=.\01_Generator\PortsModuleCfg.xml"
set "libFilePath=.\01_Generator\PortsModuleLibrary.dll"

set cfgFilePathStr=%basePath%%cfgFilePath%
set libFilePathStr=%basePath%%libFilePath%

OsarStandaloneModuleGenerator.exe -m %basePath% -c %cfgFilePathStr% -l %libFilePathStr% -g