/*****************************************************************************************************************************
 * @file        Ports_Types.h                                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.08.2018 14:30:44                                                                                          *
 * @brief       Implementation of module global datatypes from the "Ports" module.                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __PORTS_TYPES_H
#define __PORTS_TYPES_H

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Ports
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "stm32f4xx_hal.h"
#include "Rte_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Available application errors of the Ports
 * @details         This enumeration implements all available application return and error values within this specific
 *                  module. The Application error could be used as DET information or as return type of internal functions.
 */
typedef enum{
  PORTS_E_OK = 0,
  PORTS_E_NOT_OK = 1,
  PORTS_E_PENDING = 2,
  PORTS_E_NOT_IMPLEMENTED = 3,
  PORTS_E_GENERIC_PROGRAMMING_FAILURE = 4,
  PORTS_E_INVALID_PARAMETER = 100,
}Ports_ReturnType;

/**
 * @brief           Available application error values of the Ports
 * @details         Redefinition of the Ports_ReturnType as error type.
 *                  The Application error could be used as DET information or as return type of internal functions.
 */
typedef Ports_ReturnType Ports_ErrorType;

/**
 * @brief           Configuration data of an Port Pin
 */
typedef struct
{
  uint16          pinDefaultLevel;    /*!< Used pin default level */
  uint16          pinId;              /*!< Pin Id*/
  GPIO_TypeDef *  portId;             /*!< Pin Port Id*/
  uint32	        pinDriverMode;      /*!< Pin driver mode push pull / open drain */
  uint32          pinOutputMode;      /*!< Pin output mode pull up / down ... */
  uint32          pinSpeed;           /*!< Pin driver speed */
  uint32          pinFunction;        /*!< Pin functionality GPIO / Timer / Analog ... */
} Ports_PinConfigType;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __PORTS_TYPES_H*/
