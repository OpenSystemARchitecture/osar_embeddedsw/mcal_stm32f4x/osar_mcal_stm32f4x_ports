/*****************************************************************************************************************************
 * @file        Ports.c                                                                                                      *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        20.03.2019 09:52:10                                                                                          *
 * @brief       Generated Rte Module Interface file: Ports.c                                                                 *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Ports 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_Ports.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Ports.h"

extern Ports_PinConfigType port_ActivePinConfiguration[PORT_CNT_OF_CONFIGURED_PINS];
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define Ports_START_SEC_CODE
#include "Ports_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            76a624da5ce747549f37dfe524227e62
 */
VAR(void) Ports_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  GPIO_InitTypeDef GPIO_InitStruct;
  uint16 idx;

  for(idx = 0; idx < PORT_CNT_OF_CONFIGURED_PINS; idx++)
  {
    /* Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(port_ActivePinConfiguration[idx].portId, port_ActivePinConfiguration[idx].pinId, port_ActivePinConfiguration[idx].pinDefaultLevel);

    /* Configure GPIO pins: */
    GPIO_InitStruct.Pin = port_ActivePinConfiguration[idx].pinId;
    GPIO_InitStruct.Mode = port_ActivePinConfiguration[idx].pinDriverMode;
    GPIO_InitStruct.Pull = port_ActivePinConfiguration[idx].pinOutputMode;
    GPIO_InitStruct.Speed = port_ActivePinConfiguration[idx].pinSpeed;
    GPIO_InitStruct.Alternate = port_ActivePinConfiguration[idx].pinFunction;
    HAL_GPIO_Init(port_ActivePinConfiguration[idx].portId, &GPIO_InitStruct);
  }
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint16) uint16
 * @param[in]       VAR(Dio_PinState) Dio_PinState
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpPortsDio_Dio_SetPinState_NotOk
 * @note            Trigger: Operation Invocation
 * @uuid            200e6703bc2440c39ef73af3b7d5cd15
 */
VAR(Rte_ErrorType) Rte_Server_PpPortsDio_Dio_SetPinState( VAR(uint16) channelId, VAR(Dio_PinState) pinState )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  Rte_ErrorType retVal = RTE_E_OK;

  if(PORTS_E_NOT_OK == Dio_SetPinState(channelId, pinState))
  {
    retVal = Rte_ErrorType_PpPortsDio_Dio_SetPinState_NotOk;
  }

  return retVal;
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint16) uint16
 * @param[in,out]   P2VAR(Dio_PinState) Dio_PinState
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpPortsDio_Dio_GetPinState_NotOk
 * @note            Trigger: Operation Invocation
 * @uuid            ef0a70a701e449119b82b43c00fd06e7
 */
VAR(Rte_ErrorType) Rte_Server_PpPortsDio_Dio_GetPinState( VAR(uint16) channelId, P2VAR(Dio_PinState) pinState )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  Rte_ErrorType retVal = RTE_E_OK;

  if(PORTS_E_NOT_OK == Dio_GetPinState(channelId, pinState))
  {
    retVal = Rte_ErrorType_PpPortsDio_Dio_GetPinState_NotOk;
  }

  return retVal;
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint16) uint16
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpPortsDio_Dio_TogglePinState_NotOk
 * @note            Trigger: Operation Invocation
 * @uuid            989d50851b8243b5959dbb29fa1dc2d1
 */
VAR(Rte_ErrorType) Rte_Server_PpPortsDio_Dio_TogglePinState( VAR(uint16) channelId )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  Rte_ErrorType retVal = RTE_E_OK;

  if(PORTS_E_NOT_OK == Dio_TogglePinState(channelId))
  {
    retVal = Rte_ErrorType_PpPortsDio_Dio_GetPinState_NotOk;
  }

  return retVal;
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define Ports_STOP_SEC_CODE
#include "Ports_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

