/*****************************************************************************************************************************
 * @file        Dio.h                                                                                                        *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        17.01.2021 09:52:02                                                                                          *
 * @brief       Additional generated DIO (Digital Input / Output) file with direct access interfaces.                        *
 * @version     v.0.4.0                                                                                                      *
 * @generator   OSAR Ports Generator v.1.1.0.9                                                                               *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.9.1                                                                           *
*****************************************************************************************************************************/

#ifndef __DIO_H
#define __DIO_H

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Ports 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Ports.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------- User DIO input interface for DIO-Pin Demo_Port_0_Pin_0 ---------------------------------*/
#define DIO_DEMO_PORT_0_PIN_0                0
#define Dio_GetPinState_Demo_Port_0_Pin_0(x) Dio_GetPinState( DIO_DEMO_PORT_0_PIN_0, x )

/*--------------------------------- User DIO input interface for DIO-Pin Demo_Port_1_Pin_0 ---------------------------------*/
#define DIO_DEMO_PORT_1_PIN_0                1
#define Dio_GetPinState_Demo_Port_1_Pin_0(x) Dio_GetPinState( DIO_DEMO_PORT_1_PIN_0, x )

/*--------------------------------- User DIO input interface for DIO-Pin Demo_Port_2_Pin_0 ---------------------------------*/
#define DIO_DEMO_PORT_2_PIN_0                2
#define Dio_GetPinState_Demo_Port_2_Pin_0(x) Dio_GetPinState( DIO_DEMO_PORT_2_PIN_0, x )

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __DIO_H*/
