﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;

namespace PortsXsd
{
  public enum PortPinMode
  {
    PORTPIN_INPUT,
    PORTPIN_OUTPUT,
    PORTPIN_ANALOG,
    PORTPIN_ALTERNATE_FUNCTION,
  }

  public enum PortPinAlternateFunction
  {
    PORTPIN_ALTERNATE_FNCTION_0,
    PORTPIN_ALTERNATE_FNCTION_1,
    PORTPIN_ALTERNATE_FNCTION_2,
    PORTPIN_ALTERNATE_FNCTION_3,
    PORTPIN_ALTERNATE_FNCTION_4,
    PORTPIN_ALTERNATE_FNCTION_5,
    PORTPIN_ALTERNATE_FNCTION_6,
    PORTPIN_ALTERNATE_FNCTION_7,
    PORTPIN_ALTERNATE_FNCTION_8,
    PORTPIN_ALTERNATE_FNCTION_9,
    PORTPIN_ALTERNATE_FNCTION_10,
    PORTPIN_ALTERNATE_FNCTION_11,
    PORTPIN_ALTERNATE_FNCTION_12,
    PORTPIN_ALTERNATE_FNCTION_13,
    PORTPIN_ALTERNATE_FNCTION_14,
    PORTPIN_ALTERNATE_FNCTION_15,
  }

  public enum PortPinSignalType
  {
    PORTPIN_NORMAL,
    PORTPIN_INTERRUPT_RISING,
    PORTPIN_INTERRUPT_FALLING,
    PORTPIN_INTERRUPT_RISING_FALLING,

    PORTPIN_EVENT_RISING,
    PORTPIN_EVENT_FALLING,
    PORTPIN_EVENT_RISING_FALLING,
  }

  public enum PortPinDriverMode
  {
    PORTPIN_PUSH_PULL,
    PORTPIN_OPEN_DRAIN
  }

  public enum PortPinSpeed
  {
    PORTPIN_FREQ_LOW,
    PORTPIN_FREQ_MEDIUM,
    PORTPIN_FREQ_HIGH,
    PORTPIN_FREQ_VERY_HIGH,
  }

  public enum PortPinOutputMode
  {
    PORTPIN_PULLUP,
    PORTPIN_PULLDOWN,
    PORTPIN_NOPULL,
  }

  public enum PortsAvailable
  {
    PORTA,
    PORTB,
    PORTC,
    PORTD,
    PORTE,
    PORTF,
    PORTG,
    PORTH,
    PORTI,
  }

  public enum DioPinState
  {
    DIO_RESET,
    DIO_SET,
  }
  
  public struct Ports_PinCfg
  {
    public String                   pinName;
    public Byte                     pinId;
    public PortPinMode              pinMode;
    public PortPinSignalType        pinSignalType;
    public PortPinDriverMode        pinDriverMode;
    public PortPinSpeed             pinSpeed;
    public PortPinOutputMode        pinOutputMode;
    public PortPinAlternateFunction pinAlternateFnc;
    public DioPinState              pinDefaultState;
  }

  public struct Ports_PortCfg
  {
    public PortsAvailable usedPort;
    public List<Ports_PinCfg> portPinCfgList;
  }

  public class PortsXml
  {
    public XmlFileVersion xmlFileVersion;
    public UInt16 detModuleID;
    public SystemState detModuleUsage;

    public List<Ports_PortCfg> portCfgList;
  }
}
