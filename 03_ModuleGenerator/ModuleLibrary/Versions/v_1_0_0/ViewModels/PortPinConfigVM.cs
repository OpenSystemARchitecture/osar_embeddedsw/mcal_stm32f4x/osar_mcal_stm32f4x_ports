﻿/*****************************************************************************************************************************
 * @file        PortPinVM.cs                                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Port Pin Configuration                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using ModuleLibrary.Versions.v_1_0_0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  public class PortPinConfigVM : BaseViewModel
  {
    PortsAvailable usedPort;
    Ports_PinCfg usedPinCfg;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="port"></param>
    /// <param name="pinCfg"></param>
    /// <param name="storeCfgDel"></param>
    public PortPinConfigVM(PortsAvailable port, Ports_PinCfg pinCfg, StoreConfiguration storeCfgDel) : base(storeCfgDel)
    {
      usedPort = port;
      usedPinCfg = pinCfg;
    }

    /// <summary>
    /// Interface for Attribute object usedPort
    /// </summary>
    public PortsAvailable AssignedPort
    {
      get => usedPort;
      set => usedPort = value;
    }

    /// <summary>
    /// Interface for Attribute object usedPinCfg
    /// </summary>
    public Ports_PinCfg PinCfg
    {
      get => usedPinCfg;
      set => usedPinCfg = value;
    }

    /// <summary>
    /// Interface for Attribute object usedPinCfg.pinName
    /// </summary>
    public String PinName
    {
      get => usedPinCfg.pinName;
      set => usedPinCfg.pinName = value;
    }

    /// <summary>
    /// Interface for Attribute object usedPinCfg.pinId
    /// </summary>
    public Byte PinId
    {
      get => usedPinCfg.pinId;
      set => usedPinCfg.pinId = value;
    }

    /// <summary>
    /// Interface for Attribute object usedPinCfg.pinMode
    /// </summary>
    public PortPinMode PinMode
    {
      get => usedPinCfg.pinMode;
      set => usedPinCfg.pinMode = value;
    }

    /// <summary>
    /// Interface for Attribute object usedPinCfg.pinSignalType
    /// </summary>
    public PortPinSignalType PinSignalType
    {
      get => usedPinCfg.pinSignalType;
      set => usedPinCfg.pinSignalType = value;
    }

    /// <summary>
    /// Interface for Attribute object usedPinCfg.pinDriverMode
    /// </summary>
    public PortPinDriverMode PinDriverMode
    {
      get => usedPinCfg.pinDriverMode;
      set => usedPinCfg.pinDriverMode = value;
    }

    /// <summary>
    /// Interface for Attribute object usedPinCfg.pinSpeed
    /// </summary>
    public PortPinSpeed PinSpeed
    {
      get => usedPinCfg.pinSpeed;
      set => usedPinCfg.pinSpeed = value;
    }

    /// <summary>
    /// Interface for Attribute object usedPinCfg.pinOutputMode
    /// </summary>
    public PortPinOutputMode PinOutputMode
    {
      get => usedPinCfg.pinOutputMode;
      set => usedPinCfg.pinOutputMode = value;
    }

    /// <summary>
    /// Interface for Attribute object usedPinCfg.pinAlternateFnc
    /// </summary>
    public PortPinAlternateFunction PinAlternateFnc
    {
      get => usedPinCfg.pinAlternateFnc;
      set => usedPinCfg.pinAlternateFnc = value;
    }

    /// <summary>
    /// Interface for Attribute object usedPinCfg.pinDefaultState
    /// </summary>
    public DioPinState PinDefaultState
    {
      get => usedPinCfg.pinDefaultState;
      set => usedPinCfg.pinDefaultState = value;
    }

  }
}
