﻿/*****************************************************************************************************************************
 * @file        Module_ViewModel.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Module Xml Cfg Class                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;
using System.Xml.Serialization;
using System.IO;
using OsarResources.Generator;

using System.Collections.ObjectModel;
using System.ComponentModel;
using ModuleLibrary.Versions.v_1_0_0.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  /// <summary>
  /// Delegate to store the Configuration
  /// </summary>
  public delegate void StoreConfiguration();

  public class Module_ViewModel : BaseViewModel, OsarModuleGeneratorInterface
  {
    private static Models.PortsXml PortsXmlCfg = new Models.PortsXml();
    private static string pathToConfiguratioFile;
    private static string pathToModuleBaseFolder;
    private static StoreConfiguration storeCfg = new StoreConfiguration(SaveConfigToXml);
    private static ObservableCollection<PortPinConfigVM> portPinObservableCollection;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"> Should be a absolute Path</param>
    /// <param name="absPathToBaseModuleFolder"> Should be a absolute Path</param>
    public Module_ViewModel(string pathToCfgFile, string absPathToBaseModuleFolder) : base(storeCfg)
    {
      /* Check if config file path is an rooted one */
      if (true == Path.IsPathRooted(pathToCfgFile))
      {
        pathToConfiguratioFile = pathToCfgFile;
      }
      else
      {
        pathToConfiguratioFile = absPathToBaseModuleFolder + pathToCfgFile;
      }

      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      /* Read Configuration File */
      if (!File.Exists(pathToConfiguratioFile))
      {
        // If config file does not exist, create a default file
        SetDefaultConfiguration();
      }

      /* Read Configuration File */
      ReadConfigFromXml();

      // Initialize port pin list
      portPinObservableCollection = new ObservableCollection<PortPinConfigVM>();
      foreach ( Ports_PortCfg port in PortsXmlCfg.portCfgList )
      {
        foreach ( Ports_PinCfg pin in port.portPinCfgList )
        {
          portPinObservableCollection.Add(new PortPinConfigVM(port.usedPort, pin, storeCfg));
        }
      }
    }

    #region public attributes
    /// <summary>
    /// Interface for the XML File Version
    /// </summary>
    public XmlFileVersion XmlFileVersion
    {
      get { return PortsXmlCfg.xmlFileVersion; }
      set { PortsXmlCfg.xmlFileVersion = value; }
    }

    /// <summary>
    /// Interface for the XML File Version string
    /// </summary>
    public string XmlFileVersion_String
    {
      get
      {
        return ( "v." + PortsXmlCfg.xmlFileVersion.MajorVersion.ToString() + "." +
          PortsXmlCfg.xmlFileVersion.MinorVersion.ToString() + "." +
          PortsXmlCfg.xmlFileVersion.PatchVersion.ToString() );
      }
    }

    /// <summary>
    /// Interface for the Det Module Id
    /// </summary>
    public UInt16 DetModuleID
    {
      get { return PortsXmlCfg.detModuleID; }
      set { PortsXmlCfg.detModuleID = value; }
    }

    /// <summary>
    /// Interface for the Det Module Usage
    /// </summary>
    public SystemState DetModuleUsage
    {
      get { return PortsXmlCfg.detModuleUsage; }
      set { PortsXmlCfg.detModuleUsage = value; }
    }

    /// <summary>
    /// Interface for the Module Version
    /// </summary>
    public string ModuleVersion
    {
      get { return Generator.DefResources.ModuleVersion; }
    }

    /// <summary>
    /// Interface for the port pin list
    /// </summary>
    public ObservableCollection<PortPinConfigVM> PortPinList
    {
      get => portPinObservableCollection;
      set => portPinObservableCollection = value;
    }
    #endregion

    #region UI Actions
    /// <summary>
    /// Interface to ass a new pin configuration
    /// </summary>
    public void AddNewBaseModule()
    {
      portPinObservableCollection.Add(new PortPinConfigVM(PortsAvailable.PORTA, new Ports_PinCfg(), storeCfg));
      OnPropertyChanged(new PropertyChangedEventArgs("PortPinList"));
    }

    /// <summary>
    /// Interface to remove a specific port pin configuration
    /// </summary>
    /// <param name="config"></param>
    public void RemoveBaseModule(PortPinConfigVM config)
    {
      portPinObservableCollection.Remove(config);
      OnPropertyChanged(new PropertyChangedEventArgs("PortPinList"));
    }
    #endregion

    #region Configuration Load and Storage
    /*
     * @brief       Function to store the current configuration structure to an xml file
     * @param       none
     * @retval      none
     */
    private static void SaveConfigToXml()
    {
      SortPortPinListIntoConfigFile();

      XmlSerializer writer = new XmlSerializer(PortsXmlCfg.GetType());
      StreamWriter file = new StreamWriter(pathToConfiguratioFile);
      writer.Serialize(file, PortsXmlCfg);
      file.Close();
    }

    /*
     * @brief       Function to read the current configuration structure from an xml file
     * @param       none
     * @retval      none
     */
    private static void ReadConfigFromXml()
    {
      XmlSerializer reader = new XmlSerializer(PortsXmlCfg.GetType());
      StreamReader file = new StreamReader(pathToConfiguratioFile);
      PortsXmlCfg = ( Models.PortsXml)reader.Deserialize(file);
      file.Close();
    }
    #endregion

    #region Generic Osar Generator
    /// <summary>
    /// Used to validate the active configuration of its correctness.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType ValidateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfo2 = new GenInfoType(), genInfoMerge = new GenInfoType();
      ReadConfigFromXml();

      /* Ensure a correct version */
      if (null == PortsXmlCfg)
      {
        genInfo1 = SetDefaultConfiguration();
      }

      /* Call Validator */
      Generator.ModuleValidator modVal = new Generator.ModuleValidator(PortsXmlCfg, pathToConfiguratioFile);
      genInfo2 = modVal.ValidateConfiguration();

      /* Merge outputs */
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfo1, genInfo2);
      return genInfoMerge;
    }

    /// <summary>
    /// Used to generate the active configuration.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType GenerateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfo2 = new GenInfoType(), genInfoMerge = new GenInfoType();
      ReadConfigFromXml();

      /* Validate configuration */
      genInfo1 = ValidateConfiguration();

      /* If no validation error has been detected >> Start generation */
      if (0U == genInfo1.error.Count)
      {
        Generator.ModuleGenerator modGen = new Generator.ModuleGenerator(PortsXmlCfg, pathToConfiguratioFile, pathToModuleBaseFolder);
        genInfo2 = modGen.GenerateConfiguration();
      }

      /* Merge outputs */
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfo1, genInfo2);

      return genInfoMerge;
    }

    /// <summary>
    /// Used to set the default configuration.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType SetDefaultConfiguration()
    {
      GenInfoType genInfo;
      Generator.DefaultCfgGenerator defGen = new Generator.DefaultCfgGenerator(PortsXmlCfg, pathToConfiguratioFile);
      genInfo = defGen.CreateDefaultConfiguration();
      SaveConfigToXml();
      return genInfo;
    }

    /// <summary>
    /// Interface to update an outdated configuration to an up to date configuration version
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType UpdateConfigurationFileVersion()
    {
      GenInfoType genInfo;
      ReadConfigFromXml();
      Generator.CfgVersionUpdater versUpdater = new Generator.CfgVersionUpdater(PortsXmlCfg, pathToConfiguratioFile);
      genInfo = versUpdater.UpdateCfgVersion();
      SaveConfigToXml();
      return genInfo;
    }
    /// <summary>
    /// Interface to update an outdated configuration to an specific configuration version
    /// </summary>
    /// <param name="updateToVersion"> Xml file version to be updated </param>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs</returns>
    public GenInfoType UpdateConfigurationFileVersion(XmlFileVersion updateToVersion)
    {
      GenInfoType genInfo;
      ReadConfigFromXml();
      Generator.CfgVersionUpdater versUpdater = new Generator.CfgVersionUpdater(PortsXmlCfg, pathToConfiguratioFile);
      genInfo = versUpdater.UpdateCfgVersion(updateToVersion);
      SaveConfigToXml();
      return genInfo;
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function to create from port pin list the configuration
    /// </summary>
    private static void SortPortPinListIntoConfigFile()
    {
      if (null != portPinObservableCollection)
      {
        PortsXmlCfg.portCfgList.Clear();
        bool portFound;
        int portIndex;
        foreach (PortPinConfigVM portPinCfg in portPinObservableCollection)
        {
          portFound = false;
          // Search for existing port
          for (portIndex = 0; portIndex < PortsXmlCfg.portCfgList.Count(); portIndex++)
          {
            if (PortsXmlCfg.portCfgList[portIndex].usedPort == portPinCfg.AssignedPort)
            {
              // Port found >> Break search
              portFound = true;
              break;
            }
          }

          // Check if port has been found >> If not create one
          if (true == portFound)
          {
            PortsXmlCfg.portCfgList[portIndex].portPinCfgList.Add(portPinCfg.PinCfg);
          }
          else
          {
            Ports_PortCfg newPortCfg = new Ports_PortCfg();
            newPortCfg.usedPort = portPinCfg.AssignedPort;
            newPortCfg.portPinCfgList = new List<Ports_PinCfg>();
            newPortCfg.portPinCfgList.Add(portPinCfg.PinCfg);
            PortsXmlCfg.portCfgList.Add(newPortCfg);
          }
        }
      }
    }


    #endregion
  }
}
/**
 * @}
 */
