﻿/*****************************************************************************************************************************
 * @file        DefaultCfg.cs                                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Default Module Class                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generic;
using ModuleLibrary.Versions.v_1_0_0.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class DefaultCfgGenerator
  {
    private Models.PortsXml xmlCfg;
    private string pathToConfiguratioFile;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be set to default </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public DefaultCfgGenerator(Models.PortsXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to set default configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType CreateDefaultConfiguration()
    {
      int portIdx, pinidx;

      info.AddInfoMsg(DefResources.InfoMsg_StartDefaultCfgCreation + DefResources.CfgFileMajorVersion +
        "." + DefResources.CfgFileMinorVersion + "." + DefResources.CfgFilePatchVersion);
      info.AddLogMsg(DefResources.LogMsg_StartDefaultCfgCreation);

      /* Create default configuration*/
      if (null == xmlCfg)
      {
        xmlCfg = new Models.PortsXml();
      }

      // Create Configuration File Version
      xmlCfg.xmlFileVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      xmlCfg.xmlFileVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      xmlCfg.xmlFileVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);

      // Create Det Module Id
      xmlCfg.detModuleID = Convert.ToUInt16(DefResources.DefaultModuleId);

      // Create module Det usage state
      if (DefResources.DefaultModuleUsage == SystemState.STD_ON.ToString())
        xmlCfg.detModuleUsage = SystemState.STD_ON;
      else
        xmlCfg.detModuleUsage = SystemState.STD_OFF;


      /* Setup Default Port */
      portIdx = 0;
      xmlCfg.portCfgList = new List<Ports_PortCfg>();
      Ports_PortCfg portConfig;
      Ports_PinCfg pinConfig;
      for (portIdx = 0; portIdx < 3; portIdx++)
      {
        portConfig = new Ports_PortCfg();
        portConfig.portPinCfgList = new List<Ports_PinCfg>();
        for (pinidx = 0; pinidx < 1; pinidx++)
        {
          pinConfig = new Ports_PinCfg();

          pinConfig.pinName = "Demo_Port_" + portIdx.ToString() + "_Pin_" + pinidx.ToString();
          pinConfig.pinId = (byte)pinidx;
          pinConfig.pinMode = PortPinMode.PORTPIN_INPUT;
          pinConfig.pinOutputMode = PortPinOutputMode.PORTPIN_NOPULL;
          pinConfig.pinSignalType = PortPinSignalType.PORTPIN_NORMAL;
          pinConfig.pinSpeed = PortPinSpeed.PORTPIN_FREQ_LOW;
          pinConfig.pinDriverMode = PortPinDriverMode.PORTPIN_PUSH_PULL;
          pinConfig.pinAlternateFnc = PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_0;
          pinConfig.pinDefaultState = DioPinState.DIO_RESET;
          portConfig.portPinCfgList.Add(pinConfig);
        }
        xmlCfg.portCfgList.Add(portConfig);
      }

      /* Set correct port Id */
      portConfig = xmlCfg.portCfgList[0];
      portConfig.usedPort = PortsAvailable.PORTA;
      xmlCfg.portCfgList[0] = portConfig;

      portConfig = xmlCfg.portCfgList[1];
      portConfig.usedPort = PortsAvailable.PORTB;
      xmlCfg.portCfgList[1] = portConfig;

      portConfig = xmlCfg.portCfgList[2];
      portConfig.usedPort = PortsAvailable.PORTC;
      xmlCfg.portCfgList[2] = portConfig;

      info.AddLogMsg(DefResources.LogMsg_CreationDone);

      return info;
    }
  }
}
/**
 * @}
 */
