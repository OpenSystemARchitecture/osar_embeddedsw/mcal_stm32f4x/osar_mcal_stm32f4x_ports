﻿/*****************************************************************************************************************************
 * @file        ModuleValidator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Validation Class                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.XML;
using OsarResources.Generator.Resources;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class ModuleValidator
  {
    private Models.PortsXml xmlCfg;
    private string pathToConfiguratioFile;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public ModuleValidator(Models.PortsXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to validate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType ValidateConfiguration()
    {
      info.AddLogMsg(ValidateResources.LogMsg_StartOfValidation);

      /* Validate configuration file version */
      XmlFileVersion defaultVersion = new XmlFileVersion();
      defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);
      if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, xmlCfg.xmlFileVersion))
      {
        info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg);
      }

      /* Check if configuration file parameter is available */
      /* Check for at least one Port / Pin configuration */
      if (0 == xmlCfg.portCfgList.Count())
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1000), GenErrorWarningCodes.Error_1000_Msg);
      }
      else
      {
        /* Check each Port configuration */
        for (int idx = 0; idx < xmlCfg.portCfgList.Count(); idx++)
        {
          /* Check for duplicated port configurations */
          for (int idx2 = 0; idx2 < xmlCfg.portCfgList.Count(); idx2++)
          {
            /* Skipp active port in validation */
            if (idx != idx2)
            {
              if (xmlCfg.portCfgList[idx].usedPort == xmlCfg.portCfgList[idx2].usedPort)
              {
                info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1001), GenErrorWarningCodes.Error_1001_Msg + " \"" +
                   xmlCfg.portCfgList[idx].usedPort.ToString() + "\" idx1: " + idx.ToString() + " idx2: " + idx2.ToString());
              }
            }
          }

          /* Check Pin configuration */
          for (int idx2 = 0; idx2 < xmlCfg.portCfgList[idx].portPinCfgList.Count(); idx2++)
          {
            /* Check pin name */
            if (xmlCfg.portCfgList[idx].portPinCfgList[idx2].pinName == null)
            {
              info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1002), GenErrorWarningCodes.Error_1002_Msg + " \"" +
                   xmlCfg.portCfgList[idx].usedPort.ToString() + "\" Pin : " + 
                   xmlCfg.portCfgList[idx].portPinCfgList[idx2].pinId.ToString());
            }
            if (xmlCfg.portCfgList[idx].portPinCfgList[idx2].pinName == "")
            {
              info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1002), GenErrorWarningCodes.Error_1002_Msg + " \"" +
                   xmlCfg.portCfgList[idx].usedPort.ToString() + "\" Pin : " +
                   xmlCfg.portCfgList[idx].portPinCfgList[idx2].pinId.ToString());
            }
            if (xmlCfg.portCfgList[idx].portPinCfgList[idx2].pinName.Contains(" "))
            {
              info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg + " \"" +
                   xmlCfg.portCfgList[idx].usedPort.ToString() + "\" Pin : " +
                   xmlCfg.portCfgList[idx].portPinCfgList[idx2].pinId.ToString());
            }

            /* Check Each Pin in Each Port >> Pin name has to be unique */
            for (int idx3 = 0; idx3 < xmlCfg.portCfgList.Count(); idx3++)
            {
              for (int idx4 = 0; idx4 < xmlCfg.portCfgList[idx3].portPinCfgList.Count(); idx4++)
              {

                /* Skipp actual Port Pin Configuration */
                if (!( ( idx == idx3 ) && ( idx2 == idx4 ) ))
                {
                  if (xmlCfg.portCfgList[idx].portPinCfgList[idx2].pinName == xmlCfg.portCfgList[idx3].portPinCfgList[idx4].pinName)
                  {
                    info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1004), GenErrorWarningCodes.Error_1004_Msg + " \"" +
                      xmlCfg.portCfgList[idx].usedPort.ToString() +
                      "\" Pin id: " + xmlCfg.portCfgList[idx].portPinCfgList[idx2].pinId.ToString() +
                      " >> Port Cfg: \"" + xmlCfg.portCfgList[idx3].usedPort.ToString() +
                      "\" Pin id: " + xmlCfg.portCfgList[idx3].portPinCfgList[idx4].pinId.ToString());
                  }
                }

                /* Check Actual Port Configuration */
                if (( idx == idx3 ) && ( idx2 != idx4 ))
                {
                  if (xmlCfg.portCfgList[idx].portPinCfgList[idx2].pinId == xmlCfg.portCfgList[idx3].portPinCfgList[idx4].pinId)
                  {
                    info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1005), GenErrorWarningCodes.Error_1005_Msg + " \"" +
                      xmlCfg.portCfgList[idx].usedPort.ToString() +
                      "\" Pin name: " + xmlCfg.portCfgList[idx].portPinCfgList[idx2].pinName +
                      " >> Pin name: " + xmlCfg.portCfgList[idx].portPinCfgList[idx4].pinName);
                  }
                }
              }
            }
          }
        }
      }

      info.AddLogMsg(ValidateResources.LogMsg_ValidationDone);
      return info;
    }
  }
}
/**
 * @}
 */
