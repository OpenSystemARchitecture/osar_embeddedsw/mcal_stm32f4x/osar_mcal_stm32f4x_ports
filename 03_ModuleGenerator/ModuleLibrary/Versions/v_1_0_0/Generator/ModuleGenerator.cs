﻿/*****************************************************************************************************************************
 * @file        ModuleGenerator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Generator Class                                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.General;
using RteLib;
using ModuleLibrary.Versions.v_1_0_0.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class ModuleGenerator
  {
    private Models.PortsXml xmlCfg;
    private string pathToConfiguratioFile;
    private string pathToModuleBaseFolder;
    private string moduleLibPath;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    /// <param name="absPathToBaseModuleFolder"> Absolute path to module base folder</param>
    public ModuleGenerator(Models.PortsXml cfgFile, string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      moduleLibPath = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to generate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType GenerateConfiguration()
    {
      // Generate Memory-Map-Header-File
      GenerateMemoryMapHeaderFile();

      // Generate C-Source-File
      GenerateConfigurationSourceFile();

      // Generate C-Header-File
      GenerateConfigurationHeaderFile();

      // Generate DIO-Header-File
      GenerateDioHeaderFile();

      // Generate Rte-Interface-XML-File
      GenerateConfigurationRteInterfaceXmlFile();

      return info;
    }

    /// <summary>
    /// Interface to generate the Memory-Map-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    private void GenerateMemoryMapHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_StartGenMemMap);

      /* +++++ Generate Memory Mapping file +++++ */
      OsarResources.Generic.OsarGenericHelper.generateModuleMemoryMappingFile(pathToModuleBaseFolder + GenResources.GenHeaderFilePath, DefResources.ModuleName, DefResources.GeneratorName);

      info.AddLogMsg(GenResources.LogMsg_MemMapGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration C-Source-File
    /// </summary>
    /// <returns> Generation information </returns>
    private void GenerateConfigurationSourceFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenSourceCfgFile);

      CSourceFile modulePBCfgSource = new CSourceFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects sourceIncludes = new CFileIncludeObjects();
      CFileVariableObjects sourceVariables = new CFileVariableObjects();
      CFileFunctionObjects sourceFunction = new CFileFunctionObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      int actualGroupIdx;
      string tempString = "";

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Implementation of Ports module *_PBCfg.c file");
      doxygenFileHeader.AddFileName(DefResources.ModuleName + "_PBCfg.c");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupMCAL_STM32F4Cube);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      sourceIncludes.AddIncludeFile(DefResources.ModuleName + "_PBCfg.h");
      sourceIncludes.AddIncludeFile("stm32f4xx_hal.h");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare Variables ++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      generalStartGroup.AddGroupName("Map variables into constant memory");
      generalStartGroup.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CONST);
      generalEndGroup.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CONST);
      actualGroupIdx = sourceVariables.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

      /* Create Config Buffer */
      tempString = help_CreateCfgBufferContent();
      sourceVariables.AddArray("Ports_PinConfigType", "port_ActivePinConfiguration", "PORT_CNT_OF_CONFIGURED_PINS", actualGroupIdx, tempString, "const", arrayRearrangeType.STRUCT_ARRAY);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Source Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgSource.AddFileName(DefResources.ModuleName + "_PBCfg.c");
      modulePBCfgSource.AddFilePath(pathToModuleBaseFolder + GenResources.GenSourceFilePath);
      modulePBCfgSource.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgSource.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgSource.AddIncludeObject(sourceIncludes);
      modulePBCfgSource.AddGlobalVariableObject(sourceVariables);
      modulePBCfgSource.AddGlobalFunctionObject(sourceFunction);
      modulePBCfgSource.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_SourceCfgFileGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration C-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    private void GenerateConfigurationHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenHeaderCfgFile);

      CHeaderFile modulePBCfgHeader = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileDefinitionObjects headerDefinitions = new CFileDefinitionObjects();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileVariableObjects headerVariables = new CFileVariableObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      CFileTypeObjects headerTypes = new CFileTypeObjects();
      int actualGroupIdx;
      int cntOfConfiguredPins, portIdx;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Generated header file data of the Ports module.");
      doxygenFileHeader.AddFileName(DefResources.ModuleName + "_PBCfg.h");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupMCAL_STM32F4Cube);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      headerIncludes.AddIncludeFile(DefResources.ModuleName + "_Types.h");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare data types +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Prepare Definitions +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Create Det informations */
      generalStartGroup.AddGroupName("Ports Module Det Information");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      headerDefinitions.AddDefinition(GenResources.GenDefineDetModuleId,
        xmlCfg.detModuleID.ToString(), actualGroupIdx);
      headerDefinitions.AddDefinition(GenResources.GenDefineModuleUseDet,
        xmlCfg.detModuleUsage.ToString(), actualGroupIdx);

      /* Create module informations */
      generalStartGroup.AddGroupName("Module specific definitions");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

      /* Calculate cnt of configured pins */
      cntOfConfiguredPins = 0;
      for (portIdx = 0; portIdx < xmlCfg.portCfgList.Count; portIdx++)
      {
        cntOfConfiguredPins += xmlCfg.portCfgList[portIdx].portPinCfgList.Count();
      }
      headerDefinitions.AddDefinition(GenResources.GenPortsDefineCntOfConfiguredPins, cntOfConfiguredPins.ToString(), actualGroupIdx);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Header Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgHeader.AddFileName(DefResources.ModuleName + "_PBCfg.h");
      modulePBCfgHeader.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
      modulePBCfgHeader.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgHeader.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgHeader.AddDefinitionObject(headerDefinitions);
      modulePBCfgHeader.AddIncludeObject(headerIncludes);
      modulePBCfgHeader.AddGlobalVariableObject(headerVariables);
      modulePBCfgHeader.AddTypesObject(headerTypes);
      modulePBCfgHeader.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_HeaderCfgFileGenerated);
    }

    /// <summary>
    /// Interface to generate the DIO header file
    /// </summary>
    private void GenerateDioHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenDioHeaderFile);

      CHeaderFile modulePBCfgHeader = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileDefinitionObjects headerDefinitions = new CFileDefinitionObjects();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileVariableObjects headerVariables = new CFileVariableObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      CFileTypeObjects headerTypes = new CFileTypeObjects();
      int actualGroupIdx;
      int portIdx, pinIdx, dioPinIdx;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Additional generated DIO (Digital Input / Output) file with direct access interfaces.");
      doxygenFileHeader.AddFileName("Dio.h");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupMCAL_STM32F4Cube);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      headerIncludes.AddIncludeFile(DefResources.ModuleName + ".h");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare data types +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Prepare Definitions +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Create User DIO Interfaces  */
      /* Iterate over all configured ports */
      dioPinIdx = 0;
      for (portIdx = 0; portIdx < xmlCfg.portCfgList.Count(); portIdx++)
      {
        /* iterate over all pins */
        for (pinIdx = 0; pinIdx < xmlCfg.portCfgList[portIdx].portPinCfgList.Count(); pinIdx++)
        {
          if (PortPinMode.PORTPIN_OUTPUT == xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinMode)
          {
            generalStartGroup.AddGroupName("User DIO output interfaces for DIO-Pin " + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName);
            actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

            headerDefinitions.AddDefinition("DIO_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName.ToUpper(), dioPinIdx.ToString(), actualGroupIdx);
            headerDefinitions.AddDefinition("Dio_SetPinState_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName + "(x)",
              "Dio_SetPinState( DIO_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName.ToUpper() + ", x )", actualGroupIdx);
            headerDefinitions.AddDefinition("Dio_GetPinState_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName + "(x)",
              "Dio_GetPinState( DIO_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName.ToUpper() + ", x )", actualGroupIdx);
            headerDefinitions.AddDefinition("Dio_TogglePinState_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName + "()",
              "Dio_TogglePinState( DIO_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName.ToUpper() + " )\r\n", actualGroupIdx);
          }

          if (PortPinMode.PORTPIN_INPUT == xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinMode)
          {
            generalStartGroup.AddGroupName("User DIO input interface for DIO-Pin " + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName);
            actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

            headerDefinitions.AddDefinition("DIO_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName.ToUpper(), dioPinIdx.ToString(), actualGroupIdx);
            headerDefinitions.AddDefinition("Dio_GetPinState_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName + "(x)",
              "Dio_GetPinState( DIO_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinName.ToUpper() + ", x )\r\n", actualGroupIdx);
          }

          dioPinIdx++;
        }
      }
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Header Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgHeader.AddFileName("Dio.h");
      modulePBCfgHeader.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
      modulePBCfgHeader.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgHeader.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgHeader.AddDefinitionObject(headerDefinitions);
      modulePBCfgHeader.AddIncludeObject(headerIncludes);
      modulePBCfgHeader.AddGlobalVariableObject(headerVariables);
      modulePBCfgHeader.AddTypesObject(headerTypes);
      modulePBCfgHeader.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_GenDioHeaderFileGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration Rte-Interface-XML-File
    /// </summary>
    /// <returns> Generation information </returns>
    private void GenerateConfigurationRteInterfaceXmlFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenRteMibCfgFile);
      info.AddLogMsg("Using RteLib version: v." + RteLib.VersionClass.major.ToString() + "." +
        RteLib.VersionClass.minor.ToString() + "." + RteLib.VersionClass.patch.ToString());

      // Create main instance of internal behavior file
      RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior internalBehavior = new RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior();
      RteLib.RteModuleInternalBehavior.RteRunnable initRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      RteLib.RteModuleInternalBehavior.RteCyclicRunnable mainfunctionRunnable = new RteLib.RteModuleInternalBehavior.RteCyclicRunnable();

      RteLib.RteTypes.RteAdvancedDataTypeStructure adt_DioStateType = new RteLib.RteTypes.RteAdvancedDataTypeStructure();

      // Create Interface Blueprint Instances
      // gii == General Interface Info
      // ibp == Interface Blue Print
      // cs == Client Server
      RteLib.RteInterface.RteCSInterfaceBlueprint cs_ipb_PiPortsDio = new RteLib.RteInterface.RteCSInterfaceBlueprint();
      RteLib.RteInterface.RteGeneralInterfaceInfo gii_ibp_PiPortsDio = new RteLib.RteInterface.RteGeneralInterfaceInfo();

      // Create Port information
      RteLib.RteModuleInternalBehavior.RteAvailableServerPort csI_PpPortsDio = new RteLib.RteModuleInternalBehavior.RteAvailableServerPort();

      // Helper Objects
      RteLib.RteInterface.RteGeneralInterfaceInfo generalInterfaceInfo;
      RteLib.RteInterface.RteGeneralInterfaceInfo runabbleInfo;
      RteLib.RteModuleInternalBehavior.RteRunnable rteRunnable;
      RteLib.RteInterface.RteFunctionBlueprint rteFncBlueprint;
      RteLib.RteInterface.RteFunctionArgumentBlueprint rteFncArgBlueprint;
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++ Create Types ++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      #region Adding Rte Types
      RteLib.RteTypes.RteBaseTypesResource rteBaseTypesResource = new RteLib.RteTypes.RteBaseTypesResource();

      /* ########## Adding Advanced Data Type "Dio_PinState" ########## */
      RteLib.RteTypes.RteBaseDataTypeStructure t_Dio_PinStateType = new RteLib.RteTypes.RteBaseDataTypeStructure();
      t_Dio_PinStateType.DataTypeName = "Dio_PinState";
      t_Dio_PinStateType.UUID = "f1092e161cfd4338b9d96992d5d9a1d5";
      t_Dio_PinStateType.DataAccessType = RteLib.RteTypes.RteDataAccessType.STANDARD;

      adt_DioStateType.DataType = t_Dio_PinStateType;
      adt_DioStateType.DataTypeType = RteLib.RteTypes.RteAdvancedDataTypeType.ENUMERATION;
      adt_DioStateType.EnumElements = new List<RteLib.RteTypes.RteAdvancedEnumDataTypeStructure>();

      RteLib.RteTypes.RteAdvancedEnumDataTypeStructure aedt_element = new RteLib.RteTypes.RteAdvancedEnumDataTypeStructure();
      aedt_element.EnumElementName = "DIO_LOW";
      aedt_element.EnumId = "0";
      adt_DioStateType.EnumElements.Add(aedt_element);

      aedt_element = new RteLib.RteTypes.RteAdvancedEnumDataTypeStructure();
      aedt_element.EnumElementName = "DIO_HIGH";
      aedt_element.EnumId = "1";
      adt_DioStateType.EnumElements.Add(aedt_element);
      #endregion
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Create Blueprint Interfaces CS +++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ########## Adding Server Port Interface Blueprint "PiPortsDio" ########## */
      #region Adding Server Port Interface Blueprint "PiPortsDio"
      // Create general prototype information
      gii_ibp_PiPortsDio.InterfaceName = "PiPortsDio";
      gii_ibp_PiPortsDio.UUID = "b4863095aea64487a3a35f5838cbe8b6";
      cs_ipb_PiPortsDio.InterfaceInfo = gii_ibp_PiPortsDio; //Adding information to PiPortsDio Interface Blueprint
      cs_ipb_PiPortsDio.Functions = new List<RteLib.RteInterface.RteFunctionBlueprint>();

      #region Function Dio_SetPinState
      rteFncBlueprint = new RteLib.RteInterface.RteFunctionBlueprint();
      rteFncBlueprint.ReturnValue = rteBaseTypesResource.Rte_ErrorType_Type.DataType;
      rteFncBlueprint.AvailableErrorTypes = new List<string>();
      rteFncBlueprint.AvailableErrorTypes.Add("NotOk");
      rteFncBlueprint.FunctionName = "Dio_SetPinState";
      rteFncBlueprint.UUID = "200e6703bc2440c39ef73af3b7d5cd15";

      rteFncBlueprint.ArgList = new List<RteLib.RteInterface.RteFunctionArgumentBlueprint>();
      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "channelId";
      rteFncArgBlueprint.ArgumentType = new RteLib.RteTypes.RteBaseDataTypeStructure(rteBaseTypesResource.Uint16_Type);
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.STANDARD;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "pinState";
      rteFncArgBlueprint.ArgumentType = new RteLib.RteTypes.RteBaseDataTypeStructure(adt_DioStateType.DataType);
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.STANDARD;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      cs_ipb_PiPortsDio.Functions.Add(rteFncBlueprint);
      #endregion

      #region Function Dio_GetPinState
      rteFncBlueprint = new RteLib.RteInterface.RteFunctionBlueprint();
      rteFncBlueprint.ReturnValue = rteBaseTypesResource.Rte_ErrorType_Type.DataType;
      rteFncBlueprint.AvailableErrorTypes = new List<string>();
      rteFncBlueprint.AvailableErrorTypes.Add("NotOk");
      rteFncBlueprint.FunctionName = "Dio_GetPinState";
      rteFncBlueprint.UUID = "ef0a70a701e449119b82b43c00fd06e7";

      rteFncBlueprint.ArgList = new List<RteLib.RteInterface.RteFunctionArgumentBlueprint>();
      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "channelId";
      rteFncArgBlueprint.ArgumentType = new RteLib.RteTypes.RteBaseDataTypeStructure(rteBaseTypesResource.Uint16_Type);
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.STANDARD;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "pinState";
      rteFncArgBlueprint.ArgumentType = new RteLib.RteTypes.RteBaseDataTypeStructure(adt_DioStateType.DataType);
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.POINTER;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      cs_ipb_PiPortsDio.Functions.Add(rteFncBlueprint);
      #endregion

      #region Function Dio_TogglePinState
      rteFncBlueprint = new RteLib.RteInterface.RteFunctionBlueprint();
      rteFncBlueprint.ReturnValue = rteBaseTypesResource.Rte_ErrorType_Type.DataType;
      rteFncBlueprint.AvailableErrorTypes = new List<string>();
      rteFncBlueprint.AvailableErrorTypes.Add("NotOk");
      rteFncBlueprint.FunctionName = "Dio_TogglePinState";
      rteFncBlueprint.UUID = "989d50851b8243b5959dbb29fa1dc2d1";

      rteFncBlueprint.ArgList = new List<RteLib.RteInterface.RteFunctionArgumentBlueprint>();
      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "channelId";
      rteFncArgBlueprint.ArgumentType = new RteLib.RteTypes.RteBaseDataTypeStructure(rteBaseTypesResource.Uint16_Type);
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.STANDARD;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      cs_ipb_PiPortsDio.Functions.Add(rteFncBlueprint);
      #endregion


      #endregion
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Create Blueprint Interfaces SR +++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create CS Port Interfaces ++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ########## Adding Server Port "PpPortsDio" ########## */
      #region Adding Server Port "PpPortsDio"
      generalInterfaceInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      generalInterfaceInfo.InterfaceName = "PpPortsDio";
      generalInterfaceInfo.UUID = "96615323001f40d6a942a41e041866e0";

      RteLib.RteInterface.RteCSInterfacePrototype csIpt_PpPortsDio = new RteLib.RteInterface.RteCSInterfacePrototype();
      csIpt_PpPortsDio.InterfacePrototype = generalInterfaceInfo;
      csIpt_PpPortsDio.InterfaceBlueprint = gii_ibp_PiPortsDio;
      csIpt_PpPortsDio.InterfaceType = RteLib.RteInterface.RteCSInterfaceImplementationType.SERVER;
      csI_PpPortsDio.ServerPort = csIpt_PpPortsDio; //Adding information to PpPortsDio Port

      // Create Runnable Information
      csI_PpPortsDio.ServerPortRunnables = new List<RteLib.RteModuleInternalBehavior.RteRunnable>();
      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = "Dio_SetPinState";
      runabbleInfo.UUID = "200e6703bc2440c39ef73af3b7d5cd15";
      rteRunnable.RunnableInfo = runabbleInfo;
      csI_PpPortsDio.ServerPortRunnables.Add(rteRunnable); //Adding information to PpPortsDio Port

      // Create Runnable Information
      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = "Dio_GetPinState";
      runabbleInfo.UUID = "ef0a70a701e449119b82b43c00fd06e7";
      rteRunnable.RunnableInfo = runabbleInfo;
      csI_PpPortsDio.ServerPortRunnables.Add(rteRunnable); //Adding information to PpPortsDio Port

      // Create Runnable Information
      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = "Dio_TogglePinState";
      runabbleInfo.UUID = "989d50851b8243b5959dbb29fa1dc2d1";
      rteRunnable.RunnableInfo = runabbleInfo;
      csI_PpPortsDio.ServerPortRunnables.Add(rteRunnable); //Adding information to PpPortsDio Port

      #endregion
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create SR Port Interfaces ++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Create Init Runnable ++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = GenResources.RteMib_InitRunnableName;
      runabbleInfo.UUID = GenResources.RteMib_InitRunnableName_UUID;

      initRunnable.RunnableInfo = runabbleInfo;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create Cyclic Runnable +++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      internalBehavior.ModuleName = DefResources.ModuleName;
      internalBehavior.ModuleType = RteLib.RteConfig.RteConfigModuleTypes.MCAL;
      internalBehavior.UUID = GenResources.RteMib_Module_UUID;
      internalBehavior.InitRunnables.Add(initRunnable);

      internalBehavior.AvailableServerPorts.Add(csI_PpPortsDio);
      internalBehavior.BackupCSBlueprintList.Add(cs_ipb_PiPortsDio);
      internalBehavior.BackupAdvancedDataTypeList.Add(rteBaseTypesResource.Rte_ErrorType_Type);
      internalBehavior.BackupAdvancedDataTypeList.Add(adt_DioStateType);
      

      internalBehavior.SaveActiveRteModuleInternalBehaviorToXml(pathToModuleBaseFolder + GenResources.GenGeneratorFilePath + "\\" + GenResources.RteCfgFileName);

      info.AddLogMsg(GenResources.LogMsg_RteMibCfgFileGenerated);
    }

    /*
     * @brief       Helper function to create struct init data of configuration buffer
     * @param       none
     * @retval      string with init data 
     */
    private string help_CreateCfgBufferContent()
    {
      string content = "";
      int portIdx, pinIdx, cfgIdx = 0;

      /* Iterate over all configured ports */
      for (portIdx = 0; portIdx < xmlCfg.portCfgList.Count(); portIdx++)
      {
        /* iterate over all pins */
        for (pinIdx = 0; pinIdx < xmlCfg.portCfgList[portIdx].portPinCfgList.Count(); pinIdx++)
        {
          /* Create start string */
          if (0 == cfgIdx)
          {
            content += "{";
          }
          else
          {
            content += ", {";
          }

          /* ++++++++++ Build struct content ++++++++++ */
          /* Set reset state */
          switch (xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinDefaultState)
          {
            case DioPinState.DIO_RESET:
            content += "GPIO_PIN_RESET, ";
            break;

            case DioPinState.DIO_SET:
            content += "GPIO_PIN_SET, ";
            break;
          }

          /* Set GPIO Pin */
          content += "GPIO_PIN_" + xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinId.ToString() + ", ";

          /* Set GPIO Port */
          switch (xmlCfg.portCfgList[portIdx].usedPort)
          {
            case PortsAvailable.PORTA:
            content += "GPIOA, ";
            break;
            case PortsAvailable.PORTB:
            content += "GPIOB, ";
            break;
            case PortsAvailable.PORTC:
            content += "GPIOC, ";
            break;
            case PortsAvailable.PORTD:
            content += "GPIOD, ";
            break;
            case PortsAvailable.PORTE:
            content += "GPIOE, ";
            break;
            case PortsAvailable.PORTF:
            content += "GPIOF, ";
            break;
            case PortsAvailable.PORTG:
            content += "GPIOG, ";
            break;
            case PortsAvailable.PORTH:
            content += "GPIOH, ";
            break;
            case PortsAvailable.PORTI:
            content += "GPIOI, ";
            break;
          }

          /* Set GPIO Driver Mode */
          switch (xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinMode)
          {
            case PortPinMode.PORTPIN_INPUT:
            switch (xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinSignalType)
            {
              case PortPinSignalType.PORTPIN_EVENT_FALLING:
              content += "GPIO_MODE_EVT_FALLING, ";
              break;
              case PortPinSignalType.PORTPIN_EVENT_RISING:
              content += "GPIO_MODE_EVT_RISING, ";
              break;
              case PortPinSignalType.PORTPIN_EVENT_RISING_FALLING:
              content += "GPIO_MODE_EVT_RISING_FALLING, ";
              break;
              case PortPinSignalType.PORTPIN_INTERRUPT_FALLING:
              content += "GPIO_MODE_IT_FALLING, ";
              break;
              case PortPinSignalType.PORTPIN_INTERRUPT_RISING:
              content += "GPIO_MODE_IT_RISING, ";
              break;
              case PortPinSignalType.PORTPIN_INTERRUPT_RISING_FALLING:
              content += "GPIO_MODE_IT_RISING_FALLING, ";
              break;
              case PortPinSignalType.PORTPIN_NORMAL:
              content += "GPIO_MODE_INPUT, ";
              break;
            }
            break;

            case PortPinMode.PORTPIN_OUTPUT:
            switch (xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinDriverMode)
            {
              case PortPinDriverMode.PORTPIN_OPEN_DRAIN:
              content += "GPIO_MODE_OUTPUT_OD, ";
              break;
              case PortPinDriverMode.PORTPIN_PUSH_PULL:
              content += "GPIO_MODE_OUTPUT_PP, ";
              break;
            }
            break;

            case PortPinMode.PORTPIN_ANALOG:
            content += "GPIO_MODE_ANALOG, ";
            break;

            case PortPinMode.PORTPIN_ALTERNATE_FUNCTION:
            switch (xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinDriverMode)
            {
              case PortPinDriverMode.PORTPIN_OPEN_DRAIN:
              content += "GPIO_MODE_AF_OD, ";
              break;
              case PortPinDriverMode.PORTPIN_PUSH_PULL:
              content += "GPIO_MODE_AF_PP, ";
              break;
            }
            break;
          }

          /* Set GPIO Output mode */
          switch (xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinOutputMode)
          {
            case PortPinOutputMode.PORTPIN_NOPULL:
            content += "GPIO_NOPULL, ";
            break;
            case PortPinOutputMode.PORTPIN_PULLDOWN:
            content += "GPIO_PULLDOWN, ";
            break;
            case PortPinOutputMode.PORTPIN_PULLUP:
            content += "GPIO_PULLUP, ";
            break;
          }

          /* Set Driver speed */
          switch (xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinSpeed)
          {
            case PortPinSpeed.PORTPIN_FREQ_LOW:
            content += "GPIO_SPEED_FREQ_LOW, ";
            break;

            case PortPinSpeed.PORTPIN_FREQ_MEDIUM:
            content += "GPIO_SPEED_FREQ_MEDIUM, ";
            break;

            case PortPinSpeed.PORTPIN_FREQ_HIGH:
            content += "GPIO_SPEED_FREQ_HIGH, ";
            break;

            case PortPinSpeed.PORTPIN_FREQ_VERY_HIGH:
            content += "GPIO_SPEED_FREQ_VERY_HIGH, ";
            break;

          }

          /* Set GPIO alternate function */
          switch (xmlCfg.portCfgList[portIdx].portPinCfgList[pinIdx].pinAlternateFnc)
          {
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_0:
            content += "0}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_1:
            content += "1}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_2:
            content += "2}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_3:
            content += "3}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_4:
            content += "4}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_5:
            content += "5}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_6:
            content += "6}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_7:
            content += "7}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_8:
            content += "8}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_9:
            content += "9}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_10:
            content += "10}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_11:
            content += "11}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_12:
            content += "12}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_13:
            content += "13}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_14:
            content += "14}";
            break;
            case PortPinAlternateFunction.PORTPIN_ALTERNATE_FNCTION_15:
            content += "15}";
            break;
          }
        }
      }
      return content;
    }
  }
}
/**
 * @}
 */
