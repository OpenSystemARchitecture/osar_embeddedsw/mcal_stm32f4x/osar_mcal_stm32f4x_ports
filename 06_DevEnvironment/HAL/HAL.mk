#####################################################################################################
######################################## ADDITIONAL_INCLUDES ########################################
#####################################################################################################
########## Middlewares ##########
ADDITIONAL_INCLUDES += -IHAL/Middlewares/Third_Party/FreeRTOS/Source/include
ADDITIONAL_INCLUDES += -IHAL/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS
ADDITIONAL_INCLUDES += -IHAL/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F

########## Drivers ##########
ADDITIONAL_INCLUDES += -IHAL/Drivers/STM32F4xx_HAL_Driver/Inc
ADDITIONAL_INCLUDES += -IHAL/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy
ADDITIONAL_INCLUDES += -IHAL/Drivers/CMSIS/Device/ST/STM32F4xx/Include
ADDITIONAL_INCLUDES += -IHAL/Drivers/CMSIS/Include


#####################################################################################################
######################################## ADDITIONAL_SOURCES #########################################
#####################################################################################################
########## Middlewares ##########
ADDITIONAL_SOURCES += HAL/Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang/heap_4.c
ADDITIONAL_SOURCES += HAL/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c
ADDITIONAL_SOURCES += HAL/Middlewares/Third_Party/FreeRTOS/Source/croutine.c
ADDITIONAL_SOURCES += HAL/Middlewares/Third_Party/FreeRTOS/Source/event_groups.c
ADDITIONAL_SOURCES += HAL/Middlewares/Third_Party/FreeRTOS/Source/list.c
ADDITIONAL_SOURCES += HAL/Middlewares/Third_Party/FreeRTOS/Source/queue.c
ADDITIONAL_SOURCES += HAL/Middlewares/Third_Party/FreeRTOS/Source/tasks.c
ADDITIONAL_SOURCES += HAL/Middlewares/Third_Party/FreeRTOS/Source/timers.c
ADDITIONAL_SOURCES += HAL/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.c

########## Drivers ##########
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_usart.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c
ADDITIONAL_SOURCES += HAL/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c

#####################################################################################################
########################################## ADDITIONAL_ASM ###########################################
#####################################################################################################
#ADDITIONAL_ASM

#####################################################################################################
########################################## ADDITIONAL_ASM ###########################################
#####################################################################################################
#ADDITIONAL_DEFINES