/*****************************************************************************************************************************
 * @file        Rte.c                                                                                                        *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        02.03.2019 08:07:55                                                                                          *
 * @brief       Implementation of functionalities from the "Rte" module.                                                     *
 *                                                                                                                           *
 * @details     The Rte module implements an abstraction layer for the OSAR system environment. Using the Rte layer, the     *
 *              connected module would communicate standardized interfaced. So, the connected modules are independent from   *
 *              the used hardware and the needed drivers.                                                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.1.5                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Rte.h"
#include "RteSystemApplication_Internal.h"
#include "Rte_Callout_Stubs.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Rte_START_SEC_CONST
#include "Rte_MemMap.h"
#define Rte_STOP_SEC_CONST
#include "Rte_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Rte_START_SEC_NOINIT_VAR
#include "Rte_MemMap.h"
#define Rte_STOP_SEC_NOINIT_VAR
#include "Rte_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Rte_START_SEC_INIT_VAR
#include "Rte_MemMap.h"
#define Rte_STOP_SEC_INIT_VAR
#include "Rte_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Rte_START_SEC_ZERO_INIT_VAR
#include "Rte_MemMap.h"
#define Rte_STOP_SEC_ZERO_INIT_VAR
#include "Rte_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Rte_START_SEC_CONST
#include "Rte_MemMap.h"
#define Rte_STOP_SEC_CONST
#include "Rte_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Rte_START_SEC_NOINIT_VAR
#include "Rte_MemMap.h"
#define Rte_STOP_SEC_NOINIT_VAR
#include "Rte_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Rte_START_SEC_INIT_VAR
#include "Rte_MemMap.h"
#define Rte_STOP_SEC_INIT_VAR
#include "Rte_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Rte_START_SEC_ZERO_INIT_VAR
#include "Rte_MemMap.h"
#define Rte_STOP_SEC_ZERO_INIT_VAR
#include "Rte_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define Rte_START_SEC_CODE
#include "Rte_MemMap.h"
#define Rte_STOP_SEC_CODE
#include "Rte_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
#define Rte_START_SEC_CODE
#include "Rte_MemMap.h"
/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Rte_Init( void )
{
  /* Call Rte System Application Init Tasks */
  RteSystemApplicationInitTask_Internal();

  /* Call Rte Callout Stubs for user addable init task elements */
  Rte_Startup_UserInitFunctions();
}


/**
 * @brief           Module global deinitialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system shutdown
 */
void Rte_Deinit( void )
{
  /* Call Rte Callout Stubs for user addable deinit task elements */
  Rte_Shutdown_UserDeinitFunctions();
}

#define Rte_STOP_SEC_CODE
#include "Rte_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

