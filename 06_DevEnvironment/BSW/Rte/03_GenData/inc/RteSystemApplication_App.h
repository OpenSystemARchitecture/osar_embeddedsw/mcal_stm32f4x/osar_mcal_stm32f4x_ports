/*****************************************************************************************************************************
 * @file        RteSystemApplication_App.h                                                                                   *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        20.03.2019 09:52:10                                                                                          *
 * @brief       Generated Rte System Application Header File: RteSystemApplication_App.h                                     *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

#ifndef __RTESYSTEMAPPLICATION_APP_H
#define __RTESYSTEMAPPLICATION_APP_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Cfg.h"
#include "Rte_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Rte generated system application init task.
 */
VAR(void) RteSystemApplicationInitTask_App( VAR(void) );

/**
 * @brief           Rte generated system application idle task.
 */
VAR(void) RteSystemApplicationIdleTask_App( VAR(void) );

/**
 * @brief           Rte generated system application low priority (1) task.
 * @details         Generated task scheduling: 10000ms < runnable < ---
 *                  Scheduling time: 1000ms
 */
VAR(void) RteSystemApplicationPrio1Task_App( VAR(void) );

/**
 * @brief           Rte generated system application below normal priority (2) task.
 * @details         Generated task scheduling: 1000ms < runnable < 10000ms
 *                  Scheduling time: 100ms
 */
VAR(void) RteSystemApplicationPrio2Task_App( VAR(void) );

/**
 * @brief           Rte generated system application normal priority (3) task.
 * @details         Generated task scheduling: 500ms < runnable < 1000ms
 *                  Scheduling time: 10ms
 */
VAR(void) RteSystemApplicationPrio3Task_App( VAR(void) );

/**
 * @brief           Rte generated system application above normal priority (4) task.
 * @details         Generated task scheduling: 100ms < runnable < 500ms
 *                  Scheduling time: 10ms
 */
VAR(void) RteSystemApplicationPrio4Task_App( VAR(void) );

/**
 * @brief           Rte generated system application high priority (5) task.
 * @details         Generated task scheduling: 10ms < runnable < 100ms
 *                  Scheduling time: 1ms
 */
VAR(void) RteSystemApplicationPrio5Task_App( VAR(void) );

/**
 * @brief           Rte generated system application ultra high priority (6) task.
 * @details         Generated task scheduling: 1ms < runnable < 10ms
 *                  Scheduling time: 1ms
 */
VAR(void) RteSystemApplicationPrio6Task_App( VAR(void) );

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTESYSTEMAPPLICATION_APP_H*/
