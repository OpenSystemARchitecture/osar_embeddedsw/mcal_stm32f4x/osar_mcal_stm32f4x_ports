/*****************************************************************************************************************************
 * @file        Rte_Callout_Stubs.h                                                                                          *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        20.03.2019 09:52:10                                                                                          *
 * @brief       Generated Rte Callout Stubs Header File: Rte_Callout_Stubs.h                                                 *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

#ifndef __RTE_CALLOUT_STUBS_H
#define __RTE_CALLOUT_STUBS_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Rte Generated user defined startup initialization functions
 * @note            Add here user init functions
 * @uuid            c62d6ee0328611e9b210d663bd873d93
 */
void Rte_Startup_UserInitFunctions( void );

/**
 * @brief           Rte Generated user defined shutdown deinitialization functions
 * @note            Add here user deinit functions
 * @uuid            c62d71c4328611e9b210d663bd873d93
 */
void Rte_Shutdown_UserDeinitFunctions( void );

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTE_CALLOUT_STUBS_H*/
